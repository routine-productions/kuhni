$(window).load(function () {

    $('.Partners-Slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000
    });

    $('.bxslider').bxSlider({
        auto: true,
        autoDelay: 1000
    });


});

var Today = new Date(),
    Today_Day = Today.getDate() + 2,
    Stock_Year,
    Stock_Month,
    Stock_Day;

var Set_Date = Today.setDate(Today_Day);

var Stock = new Date(Set_Date);

Stock_Year = Stock.getFullYear();
Stock_Month = Stock.getMonth() + 1;
Stock_Day = Stock.getDate();


if (Stock_Month < 10) {
    Stock_Month = '0' + Stock_Month;
}

if (Stock_Day < 10) {
    Stock_Day = '0' + Stock_Day;
}


$('.Action-Time').text(Stock_Day + '.' + Stock_Month + '.' + Stock_Year);


$('.JS-Modal-Button').click(function () {

    var Data_Title = $(this).attr('data-title');
    var Data_Button = $(this).attr('data-button');

    $('.Modal-Order input').removeClass('Active');

    $('#Modal-1').find('.Section-Title').text(Data_Title);
    $('#Modal-1').find('input[name="title"]').attr('val', Data_Title);

    $('#Modal-1').find('button').text(Data_Button);
    $('#Modal-1').find('input').val('');

    return false;
});

//
//$(window).scroll(function(){
//
// if($(window).scrollTop() > $('.Video').offset().top){
//     $('.Video video').attr('autoplay','autoplay');
// }
//});


new WOW().init();


// Play  Video
$(window).scroll(function () {
    if ($(window).scrollTop() > ($('.Video').offset().top - $(window).height())) {
        $(".Video video").get(0).play();
    }
});

// Mask
$('input[name="phone"]').mask('+7(000) 000-00-00', {
    onComplete: function (cep, Event) {
        $(Event.currentTarget).addClass('Complete');
    },
    onChange: function (cep, Event) {
        $(Event.currentTarget).removeClass('Complete');
    },
    onInvalid: function (val, Event, f, invalid, options) {
        $(Event.currentTarget).removeClass('Complete');
    }
});