/*
 *
 * code structure
 *
 *  <li class="JS-Script-Menu">
 <span class="JS-Menu-Button">
 <svg>
 <use xlink:href="../img/ico-sprite.svg#select"></use>
 </svg>
 </span>

 <div class="JS-Menu-Accordion">
 <p>

 </p>
 </div>
 </li>
 *
 *
 *
 * can i use: add classes JS-Script-Menu, JS-Menu-Button, JS-Menu-Accordion
 *
 *
 *
 */


(function ($) {

    $.fn.Accordion = function () {

//  -------------------------------------------------- Accordion -------------------------------------------------------

        var $Global_This = $(this);

        var Actual_First = $Global_This.find(".JS-Menu-Accordion:first > *").actual('outerHeight');

        $Global_This.find(".JS-Menu-Accordion:first").css({'display': 'block', 'height': Actual_First});

        $Global_This.find('.JS-Menu-Button:first svg').addClass('JS-Rotate-Arrow-180').removeClass('JS-Rotate-Arrow-0');

        $Global_This.find('.JS-Menu-Button').click(function () {

            var $Button = $(this);

            if ($Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion').is(':hidden')) {

                var Actual_Height = $Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion > *').actual('outerHeight');

                $Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion').css({
                    'height': Actual_Height,
                    'display': 'block'
                });

                $Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion').addClass("Active");

                $Global_This.find(".JS-Menu-Accordion").not($Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion')).css({'height': "0"});
                $Global_This.find(".JS-Menu-Accordion").not($Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion')).removeClass('Active');

                setTimeout(function () {
                    $(".JS-Menu-Accordion").not($Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion')).css({'display': "none"});
                }, 300);

            } else {
                function Time_Display_This() {
                    $Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion').css({'display': 'none'});
                }

                $Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion').removeClass("Active");
                $Button.parents('.JS-Script-Menu').find('.JS-Menu-Accordion').css({'height': '0px'});
                setTimeout(Time_Display_This, 300);
            }

//rotate arrow start

            if ($(this).find('svg').hasClass('JS-Rotate-Arrow-0')) {
                $Global_This.find('.JS-Rotate-Arrow-180').removeClass('JS-Rotate-Arrow-180').addClass('JS-Rotate-Arrow-0');
                $(this).find('svg').removeClass('JS-Rotate-Arrow-0');
                $(this).find('svg').addClass('JS-Rotate-Arrow-180');
            } else if ($(this).find('svg').hasClass('JS-Rotate-Arrow-180')) {
                $(this).find('svg').removeClass('JS-Rotate-Arrow-180');
                $(this).find('svg').addClass('JS-Rotate-Arrow-0');
            }
//rotate arrow end
            return false;

        });

// ----------------------------------------------------Rotate Arrow ----------------------------------------------------


    };

    $(".Questions-List").Accordion();


})(jQuery);