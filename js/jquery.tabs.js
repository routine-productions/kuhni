/*
 * Copyright (c) 2015
 * Routine JS - Tabs
 * Version 0.1.2
 * Create 2015.12.17
 * Author Bunker Labs

 * Usage:
 * add attribute 'data-tab' to the list tab and tab content
 * add class "Tab-Content-Item" to the tab content list

 * Code structure:
 *
 * <div class="JS-Tabs">
 *      <ul class="JS-Tabs-Navigation">
 *          <li><a class="JS-Tab Active" data-href="Tab-1" >Tab 1</a></li>
 *          <li><a class="JS-Tab" data-href="Tab-2">Tab 2</a></li>
 *      </ul>
 *      <ul class="JS-Tabs-Content">
 *          <li data-tab='Tab-1'>Content 1</li>
 *          <li data-tab='Tab-1'>Content 1</li>
 *          <li data-tab='Tab-2'>Content 2</li>
 *          <li data-tab='Tab-2'>Content 2</li>
 *          <li data-tab='Tab-2'>Content 2</li>
 *      </ul>
 * </div>
 */


$(document).ready(function () {

    $('.JS-Tabs').each(function () {
        var Hash = $(this).find('.JS-Tabs-Navigation .JS-Tab.Active').attr('data-href');
        $(this).find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();

    });

    $('.JS-Tabs .JS-Tabs-Navigation .JS-Tab').click(function () {

        var Hash = $(this).attr('data-href');
        $(this).parents('.JS-Tabs-Navigation').find('.JS-Tab').removeClass('Active');

        $(this).parents('.JS-Tabs').find('.JS-Tabs-Content [data-tab=' + Hash + ']').show().siblings('[data-tab !=' + Hash + ']').hide();
        $(this).addClass('Active');

        if (Hash) {
            history.pushState(null, null, "#" + Hash);
        }
    });
});


