$(document).on('click', '.Form-Action button', function () {
    var Data = $(this).parents('.Form-Action').serialize();
    var Action = $(this).parents('.Form-Action').attr('action');
    var Fields_To_Clear = $(this).parents('.Form-Action').find('[name=name],[name=phone]');

    var Is_Name = ($(this).parents('.Form-Action').find('[name=name]').val().length > 2) ? true : false;
    var Is_Phone = $(this).parents('.Form-Action').find('[name=phone]').hasClass('Complete');


    if (Is_Name && Is_Phone) {
        $(this).parents('.Form-Action .Message').text("");
        $.ajax({
            type: 'POST',
            url: Action,
            data: Data,

            success: function (Responce) {
                Fields_To_Clear.val('');

                $('#Modal-1').removeClass('Visible');

                $('[href="#Modal-2"]').trigger('click');
            }
        });
    }

    if (!Is_Name) {
        $(this).parents('.Form-Action').find('[name=name]').addClass('Error');
        $(this).parents('.Form-Action').find('.Message').text('Введите Ваше имя!');
    }

    if (!Is_Phone) {
        $(this).parents('.Form-Action').find('[name=phone]').addClass('Error');
        $(this).parents('.Form-Action').find('.Message').text('Введите Ваш телефон!');
    }

    if (!Is_Phone && !Is_Name) {
        $(this).parents('.Form-Action').find('[name=phone]').addClass('Error');
        $(this).parents('.Form-Action').find('.Message').text('Введите Ваше имя и телефон!');
    }

    return false;
});

$(document).on('click', '.Form-Action input', function () {
    $(this).removeClass('Error');
});

