/*
 * Copyright (c) 2015
 * Routine JS - Map Descroll
 * Version 0.1.1
 * Create 2015.12.04
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Map-Descroll' - to wrapper of Google Map iframe

 * Code structure:
 * <div class="JS-Map-Descroll"><iframe src='https://google.com/maps...'></iframe></div>
 */
(function ($) {
    $(document).ready(function () {
        var $Map        = $('.JS-Map-Descroll'),
            Cover_Class = 'JS-Map-Cover';

        $Map.each(function () {
            var Cover_Object = $('<div/>', {
                'class': Cover_Class,
                'click': function () {
                    $(this).hide();
                }
            }).css({
                'position': 'absolute',
                'top'     : 0,
                'left'    : 0,
                'width'   : '100%',
                'height'  : '100%',
                'z-index' : '1'
            });

            $(this).css('position', 'relative').append(Cover_Object).mouseleave(function () {
                $('.' + Cover_Class, this).show();
            });
        });
    });
})(jQuery);