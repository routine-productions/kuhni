(function () {
    var Coords = [
        [53.0906069, 46.6383625],
        [51.6568783, 39.1579876],
        [51.6376865, 39.2342789],
        [51.7023158, 39.199049],
        [50.9822353, 39.503389],
        [50.1985029, 39.5417648],
        [50.845992, 39.0348222],
        [50.617597, 38.6858379],
        [51.662303, 39.135536],
        [50.845992, 39.0348222]

    ];

    var Information = [
        ["Производство:<br>г. Кузнецк,<br>ул. Сухановская, 39", 'e-mail@kuhni.ru5', '8-800-2000-6005'],
        ["г. Воронеж,<br>ул. Космонавтов, 27г", 'e-mail@kuhni.ru1', '8-800-2000-6001'],
        ["г. Воронеж,<br>Ленинский проспект, 9", 'e-mail@kuhni.ru', '8-800-2000-600'],
        ["г. Воронеж,<br>ул. Шишкова, 72", 'e-mail@kuhni.ru2', '8-800-2000-6002'],
        ["г. Лиски,<br>ул. Коммунистическая, 21", 'e-mail@kuhni.ru3', '8-800-2000-6003'],
        ["г. Россошь,<br>ул. Пролетарская, 2", 'e-mail@kuhni.ru4', '8-800-2000-6004'],
        ["г. Острогожск,<br>ул. Каштановая,2", 'e-mail@kuhni.ru5', '8-800-2000-6005'],
        ["г. Алексеевка,<br>ул. Революционная,32ж", 'e-mail@kuhni.ru5', '8-800-2000-6005'],
        ["Склад:<br>г. Воронеж,<br>ул. Дорожная,17ж", 'e-mail@kuhni.ru5', '8-800-2000-6005'],
        ["Склад:<br>г. Острогожск,<br>ул. Каштановая, 2", 'e-mail@kuhni.ru5', '8-800-2000-6005']

    ];


    var style = [
        {
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": 33
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2e5d4"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c5dac6"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c5c6c6"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e4d7c6"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#fbfaf7"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#d7c7bb"
                }
            ]
        }
    ];

    function initialize() {
        var myLatlng = new google.maps.LatLng(51.9348861, 43.5027752);

        var myOptions = {
            zoom: 7,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: style,
            scrollwheel: false,
            // disableDefaultUI: true

            zoomControl: true,
            mapTypeControl: false,
            scaleControl: true,
            streetViewControl: false,
            rotateControl: false

        };
        var map = new google.maps.Map(document.getElementById("Google-Map"), myOptions);

        var Markers = [];

        $.each(Coords, function (index, elem) {
            var image = {
                url: '/img/geo-01.png',
                size: new google.maps.Size(50, 70)
            };

            var image_active = {
                url: '/img/geo-02.png',
                size: new google.maps.Size(50, 70)
            };

            var Coordinates = new google.maps.LatLng(Coords[index][0], Coords[index][1]);

            if (index == 0) {
                Markers[index] = new google.maps.Marker({
                    position: Coordinates,
                    map: map,
                    icon: image_active
                });
            } else {
                Markers[index] = new google.maps.Marker({
                    position: Coordinates,
                    map: map,
                    icon: image
                });
            }


            google.maps.event.addListener(Markers[index], 'click', function () {
                $.each(Markers, function (index, elem) {
                    Markers[index].setIcon(image);
                });
                Markers[index].setIcon(image_active);

                $('.Map-Town').html(Information[index][0]);
                $('.Map-Email').text(Information[index][1]);
                $('.Map-Email').attr('href', 'mailto:' + Information[index][1]);
                $('.Map-Phone').text(Information[index][2]);
                $('.Map-Phone').attr('href', 'tel:' + Information[index][2]);
            });

        });

    }


    function initialize_areas() {
        var center = new google.maps.LatLng(53.0867, 46.6576);

        var myOptions = {
            zoom: 15,
            center: center,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            // styles: style,
            scrollwheel: false,
            // disableDefaultUI: true
            zoomControl: false,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false
        };

        // Define the LatLng coordinates for the polygon.
        var triangleCoords = [
            {lat: 53.086978, lng: 46.641982},
            {lat: 53.087025, lng: 46.643726},
            {lat: 53.090142, lng: 46.642608},
            {lat: 53.092014, lng: 46.646647},
            {lat: 53.096301, lng: 46.641942},
            {lat: 53.096232, lng: 46.6384594},

            {lat: 53.094858, lng: 46.635065}


        ];

        // Construct the polygon.
        var bermudaTriangle = new google.maps.Polygon({
            paths: triangleCoords,
            strokeColor: '#FFF',
            strokeOpacity: 1,
            strokeWeight: 3,
            fillColor: '#FFF',
            fillOpacity: 0.35,
            labelContent: "$425K"
        });

        var map = new google.maps.Map(document.getElementById("Areas-Map"), myOptions);

        var mapLabel = new MapLabel({
            text: 'Завод Мебель Поволжья',
            strokeWeight: '2',
            strokeColor: '#000',
            map: map,
            fontSize: 16,
            align: 'right',
            fontColor: '#fff'
        });

        mapLabel.set('position', new google.maps.LatLng(53.092884, 46.645436));

        bermudaTriangle.setMap(map);

    }


    initialize();
    initialize_areas();


})();

