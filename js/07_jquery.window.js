/*
 * Copyright (c) 2015
 * Routine JS - Window
 * Version 0.1.1
 * Created 2015.12.22
 * Author Bunker Labs

 * Usage:
 * Add class name 'JS-Window-Button' - to activate button

 * Code structure:
 * <button class='JS-Window-Button' data-window-iframe='http://path.to/iframe'></button>
 */

(function ($) {
    $(document).ready(function () {
        var Window = '.JS-Window-Blackout',
            Window_Box = '.JS-Window',
            Window_Close = '.JS-Window-Close',
            $Window_Button = $('.JS-Window-Button'),
            Duration = 700;

        $Window_Button.click(function () {
            var Content = '';
            if ($(this).attr('data-window-iframe')) {
                Content = '<iframe src="' + $(this).attr('data-window-iframe') + '"></iframe>';
            } else if ($(this).attr('data-window-img')) {
                Content = '<img src="' +  $(this).attr("data-window-img") + '"</>';
            } else if ($(this).attr('data-window-carousel')) {
                var Items = $($(this).attr('data-window-carousel'));
                console.log(Items.parseHTML);
                var Content = '<div class="JS-Carousel-Window"><ul class="JS-Carousel-List">';
                $(Items).each(function () {
                    Content += $('<div/>')
                        .addClass('JS-Image-Align JS-Carousel-Item')
                        .append($(this).clone()).prop('outerHTML');
                });
                Content += '</ul></div>' +
                    '<script>' +
                    '$(".JS-Image-Align").JS_Image_Align();' +
                    '$(".JS-Carousel-Window").JS_Easy_Carousel({Height:"auto",Effect:"slide"});' +
                    '</script>';
            }

            $('body').append(
                '<div class="JS-Window-Blackout">' +
                '   <div class="JS-Window">' +
                '       <div class="JS-Window-Close">×</div>' +
                Content +
                '   </div>' +
                '</div>'
            );

            $(Window).delay(1).promise().done(function () {
                this.addClass('Visible');
            });

            return false;
        });

        $(document).on('click', Window_Close, function () {
            $(Window).removeClass('Visible')
                .delay(parseFloat($(this).parents(Window).css('transition-duration')) * 1000).promise().done(function () {
                $(Window).remove();
            });
        });

        $(document).on('click', Window, function () {
            $(Window).removeClass('Visible').delay(parseFloat($(this).css('transition-duration')) * 1000).promise().done(function () {
                $(Window).remove();
            });
        });

        $(document).on('click', Window_Box, function () {
            return false;
        });
    });
})(jQuery);