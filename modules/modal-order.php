<section class="JS-Modal-Blackout Modal-Section" id="Modal-1">
    <div class="Modal-Order Form JS-Modal">
        <h2 class="Section-Title"></h2>
        <form action="/php/action.php" class="Form-Action">
            <input type="hidden" name="title" value="Заявка">
            <input name="utm_medium" class="utm_medium" value="<? echo $utm_medium; ?>" type="hidden">
            <input name="utm_source" class="utm_source" value="<? echo $utm_source; ?>" type="hidden">
            <input name="utm_campaign" class="utm_campaign" value="<? echo $utm_campaign; ?>" type="hidden">
            <input name="utm_term" class="utm_term" value="<? echo $utm_term; ?>" type="hidden">
            <input name="utm_content" class="utm_content" value="<? echo $utm_content; ?>" type="hidden">
            <input name="block" class="block" value="<? echo $block; ?>" type="hidden">
            <input name="keyword" class="keyword" value="<? echo $keyword; ?>" type="hidden">
            <input name="position" class="position" value="<? echo $position; ?>" type="hidden">
            <input type="text" name="name" placeholder="Введите имя">
            <input type="text" name="phone" placeholder="Введите телефон">
            <button></button>
            <p class="Message"></p>
        </form>
        <div class="JS-Modal-Close">×</div>
    </div>
</section>


<section class="JS-Modal-Blackout Modal-Section" id="Modal-2">
    <div class="Modal-Order Form JS-Modal">
        <h2 class="Section-Title">Спасибо за заявку!</h2>
        <p>Ваша заявка успешно принята. Наш специалист перезвонит вам в течении 15 минут.</p>
        <div class="JS-Modal-Close">×</div>
    </div>
</section>
<a style="display: none;" href="#Modal-2"></a>
