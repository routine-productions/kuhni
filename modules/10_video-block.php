<section class="Video">
    <video loop poster="/img/bg-video.jpg">
        <source src="/img/Mebel_5.mp4" type='video/mp4' />
    </video>
    <div class="Video-Blackout"></div>
    <div class="Wrapper">
        <h2 class="Section-Title">
            Собственное<br> производство
        </h2>
        <p>
            Поскольку мы являемся<br>
            производителями, мы можем<br>
            контролировать процесс производства<br>
            на каждом этапе, что обеспечивает<br>
            качество нашей мебели. Посмотрите<br>
            видео и убедитесь в этом сами.
        </p>

        <button class="JS-Modal-Button" href="#Modal-1" data-title=" Заказать бесплатный расчет кухни" data-button="Оформить заказ">
            Заказать бесплатный расчет кухни
        </button>
    </div>



</section>