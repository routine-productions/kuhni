<section class="Quality">
    <div class="Wrapper">
        <h2 class="Section-Title">Достойное качество по лучшей цене</h2>

        <ul class="Quality-List">
            <li class="Quality-Item">
                <div class="Quality-Image wow slideInRight">

                </div>

                <div class="Quality-Description">
                    <div data-content="фурнитура">
                        <div class="Right">
                            <span class="Item-Title">Фурнитура</span>

                            <p>
                                Мы используем <span class="Allotment-Text">фурнитуру Немецких<br>
                    и Российских производителей, </span>что<br>
                                обеспечивает качество и <br>
                                долговечность нашей мебели.
                            </p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="Quality-Item">
                <div class="Quality-Image wow slideInLeft">

                </div>

                <div class="Quality-Description">
                    <div data-content="оборудование">
                        <div class="Left">
                            <span class="Item-Title">Оборудование</span>

                            <p>
                                Благодаря <span class="Allotment-Text">немецкому оборудованию</span><br>
                                наши кухни производятся на высочайшем
                                уровне и имеют превосходное качество.
                                Поэтому мы даем гарантию на свою
                                мебель 18 месяцев.
                            </p>
                        </div>

                    </div>
                </div>
            </li>
            <li class="Quality-Item">
                <div class="Quality-Image wow slideInRight">

                </div>

                <div class="Quality-Description">
                    <div data-content="специалисты">
                        <div class="Right">
                            <span class="Item-Title">Специалисты</span>

                            <p>
                                Наши специалисты ответственно<br>
                                подходят к выполнению монтажных<br>
                                работ, поэтому <span class="Allotment-Text">мы гарантируем подгон<br>
                    деталей при сборке до 1 миллиметра.</span>
                            </p>
                        </div>
                    </div>

                </div>
            </li>
            <li class="Quality-Item">
                <div class="Quality-Image wow slideInLeft">

                </div>

                <div class="Quality-Description">
                    <div data-content="гарантии">
                        <div class="Left">
                            <span class="Item-Title">Гарантии</span>

                            <p>
                                Поскольку мы производим мебель<br>
                                уже в собранном виде, вы можете быть
                                уверены, что <span class="Allotment-Text">все детали будут идеально<br>
                    подогнаны, не будет сколов и царапин,<br>
                    все механизмы будут работать<br>
                    идеально!</span> Мы гарантируем!
                            </p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>