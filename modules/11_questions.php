<section class="Questions">
    <h2 class="Section-Title">Частые вопросы</h2>

    <ul class="Questions-List">
        <li class="Question-Item JS-Script-Menu">
            <div class="Questions-Button JS-Menu-Button">
                <span>Могу ли я оплатить в рассрочку?</span>
                <svg class="Arrow-Question JS-Rotate-Arrow-0">
                    <use xlink:href="#arrow"></use>
                </svg>
            </div>

            <div class="Question-Text JS-Menu-Accordion" >
                <p>
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum."
                </p>
            </div>
        </li>

        <li class="Question-Item JS-Script-Menu">
            <div class="Questions-Button JS-Menu-Button">
                <span>Фурнитура каких производителей используется?</span>
                <svg class="Arrow-Question JS-Rotate-Arrow-0">
                    <use xlink:href="#arrow"></use>
                </svg>
            </div>

            <div class="Question-Text JS-Menu-Accordion" >
                <p>
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum."
                </p>
            </div>
        </li>
        <li class="Question-Item JS-Script-Menu">
            <div class="Questions-Button JS-Menu-Button">
                <span>Как быстро осуществляется доставка?</span>
                <svg class="Arrow-Question JS-Rotate-Arrow-0">
                    <use xlink:href="#arrow"></use>
                </svg>
            </div>

            <div class="Question-Text JS-Menu-Accordion" >
                <p>
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum."
                </p>
            </div>
        </li>
    </ul>
</section>
