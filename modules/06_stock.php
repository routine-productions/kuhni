<section class="Stock">
    <div class="Wrapper">
        <h2 class="Section-Title"><span class="Allotment-Text">Акции </span>и спец. предложения</h2>

        <p class="Stock-Text">оформите заказ прямо сейчас и получите <span class="Allotment-Text">бесплатно</span><br>
            один из подарков на сумму до 6 000 рублей
        </p>

        <div class="Stock-Action">
            <svg>
                <use xlink:href="#timer"></use>
            </svg>
            <span>Акция действительна <span class="Action-Time"></span></span>
        </div>

        <ul class="Stock-List">
            <li class="Stock-Item wow fadeInUp">
                <div>
                    <div class="Stock-Image">
                        <img src="/img/stock-1.jpg" alt=''>
                    </div>
                    <span class="Item-Title">Чайник</span>
                    <span class="Stock-Old">4 000 руб</span>
                    <span class="Stock-New">Бесплатно</span>
                </div>
            </li>
            <li class="Stock-Item wow fadeInUp" data-wow-delay="0.4s">
                <div>
                    <div class="Stock-Image">
                        <img src="/img/stock-2.jpg" alt=''>
                    </div>
                    <span class="Item-Title">Набор кастрюль</span>
                    <span class="Stock-Old">4 000 руб</span>
                    <span class="Stock-New">Бесплатно</span>
                </div>
            </li>
            <li class="Stock-Item wow fadeInUp" data-wow-delay="0.8s">
                <div>
                    <div class="Stock-Image">
                        <img src="/img/stock-3.jpg" alt=''>
                    </div>
                    <span class="Item-Title">Тостер</span>
                    <span class="Stock-Old">4 000 руб</span>
                    <span class="Stock-New">Бесплатно</span>
                </div>
            </li>
            <li class="Stock-Item wow fadeInUp" data-wow-delay="1.2s">
                <div>
                    <div class="Stock-Image">
                        <img src="/img/stock-4.jpg" alt=''>
                    </div>
                    <span class="Item-Title">Чайник</span>
                    <span class="Stock-Old">4 000 руб</span>
                    <span class="Stock-New">Бесплатно</span>
                </div>
            </li>
        </ul>

        <button class="Get-Present JS-Modal-Button" href="#Modal-1" data-title="Оформить заказ и получить подарок"
                data-button="Оформить заказ">
            Оформить заказ и получить подарок
        </button>
    </div>
</section>