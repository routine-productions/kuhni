<section class="Guarantees">
    <div class="Wrapper">
        <h2 class="Section-Title">
            Наши гарантии и преимущества
        </h2>
        <ul class="Guarantees-List">
            <li class="Guarantees-Item">
                <div class="Item-Content">
                    <div class="Guarantees-Image">
                        <svg>
                            <use xlink:href="#clock"></use>
                        </svg>
                    </div>
                    <div class="Guarantees-Description">
                        <h3 class="Item-Title">
                            Гарантия соблюдения сроков
                        </h3>

                        <p>Мы <span class="Allotment-Text">вернём вам 0.5% </span>от суммы<br>
                            за каждый день просрочки</p>
                    </div>
                </div>
            </li>
            <li class="Guarantees-Item">
                <div class="Item-Content">
                    <div class="Guarantees-Image">
                        <svg>
                            <use xlink:href="#calendar"></use>
                        </svg>
                    </div>
                    <div class="Guarantees-Description">
                        <h3 class="Item-Title">
                            Гарантия на мебель
                        </h3>

                        <p>
                            Мы даём гарантию <span class="Allotment-Text">18 месяцев</span><br>
                            на всю мебель
                        </p>
                    </div>
                </div>
            </li>
            <li class="Guarantees-Item">
                <div class="Item-Content">
                    <div class="Guarantees-Image">
                        <svg>
                            <use xlink:href="#truck"></use>
                        </svg>
                    </div>
                    <div class="Guarantees-Description">
                        <h3 class="Item-Title">
                            <span class="Allotment-Text">Бесплатная </span>доставка
                        </h3>

                        <p>
                            Мы доставим вашу мебель <span class="Allotment-Text">бесплатно,</span><br>
                            с гарантией и в кратчайшие сроки
                        </p>
                    </div>
                </div>
            </li>
            <li class="Guarantees-Item">
                <div class="Item-Content">
                    <div class="Guarantees-Image">
                        <svg>
                            <use xlink:href="#free"></use>
                        </svg>
                    </div>
                    <div class="Guarantees-Description">
                        <h3 class="Item-Title">
                            <span class="Allotment-Text">Бесплатный </span>выезд дизайнера
                        </h3>

                        <p>
                            Вы можете оформить бесплатный выезд<br>
                            дизайнера и увидеть свою будущую кухню<br>
                            уже сейчас
                        </p>
                    </div>
                </div>

            </li>
            <li class="Guarantees-Item">
                <div class="Item-Content">
                    <div class="Guarantees-Image">
                        <svg>
                            <use xlink:href="#fix"></use>
                        </svg>
                    </div>
                    <div class="Guarantees-Description">
                        <h3 class="Item-Title">
                            Оплата по факту установки
                        </h3>

                        <p>
                            Вам <span class="Allotment-Text">не нужно вносить предоплату, </span>вы платите<br>
                            только по факту установки всей мебели
                        </p>
                    </div>
                </div>
            </li>
            <li class="Guarantees-Item">
                <div class="Item-Content">
                    <div class="Guarantees-Image">
                        <svg>
                            <use xlink:href="#shelf"></use>
                        </svg>
                    </div>
                    <div class="Guarantees-Description">
                        <h3 class="Item-Title">
                            <span class="Allotment-Text">Бесплатное </span>хранение
                        </h3>

                        <p>
                            Бесплатное хранение на нашем складе<br>
                            в случае невозможности принять кухню<br>
                            в оговоренную дату
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>