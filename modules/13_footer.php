<section class="Footer">

    <div class="Wrapper">
        <div class="Footer-Block">
            <svg class="Footer-Logo">
                <use xlink:href="#logo-footer"></use>
            </svg>
        </div>
        <div class="Footer-Block">
            <p>
                &copy;<a href="kuhni.ru">kuhni.ru</a> Все права защищены.<br>
                Копирование материалов запрещено!
            </p>

            <p>
                ООО "Кухни ру"<br>
                ОГРН 1234566368548<br>
                ИНН/КПП 12345/8764422<br>
                ОКАТО 123456
            </p>
        </div>
        <div class="Footer-Block">
            <div class="Second-Logo">
                <img src="/img/footer-logo.png" alt="">
                <span>Marketingtime</span>
            </div>
        </div>
    </div>
</section>