<div class="Header-Wrapper Second">
    <div class="Wrapper">
        <div class="Header-Top">
            <div class="Header-Logo">
                <svg>
                    <use xlink:href="#logo"></use>
                </svg>
            </div>
            <div class="Head-Text">
                <h1>Кухни на заказ по Москве и области</h1>

                <p>Производство кухонной мебели на заказ по индивидуальному <br>
                    проекту с <span class="Allotment-Text">бесплатной</span> доставкой по Москве и области</p>
            </div>
            <div class="Header-Order">
                <a class="tel" href="tel:8&#8209;800&#8209;2000&#8209;600">8&#8209;800&#8209;2000&#8209;600</a>
                <button class="JS-Modal-Button" href="#Modal-1" data-title="Заказать выезд
                    дизайнера" data-button="Оформить заказ">
                    Заказать выезд<br>
                    дизайнера бесплатно
                </button>
            </div>
        </div>
        <div class="Header-Center">
            <h2 class="Section-Title">Оставьте заявку сейчас<br>
                и получите <span class="Allotment-Text">бесплатный</span><br>
                проект кухни в 3D
            </h2>


            <button class="JS-Modal-Button" href="#Modal-1" data-title="Оставить заявку на 3D проект"
                    data-button="Оформить заказ">Оставить заявку на 3D проект
            </button>
        </div>
    </div>
</div>