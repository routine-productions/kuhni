<section class="Map">
    <div class="">
        <div id="Google-Map">
            <div id="map-content"></div>
        </div>
    </div>

    <div class="Map-Window">
        <form class='JS-Form'
              data-form-email='onevanivan1@gmail.com'
              data-form-subject='Order form website'
              data-form-url='/public/js/data.form.php'
              data-form-method='POST'>
            <h3 class="Map-Town">Производство:<br>г. Кузнецк,<br>ул. Сухановская, 39</h3>
            <a class="Map-Email" href="mailto:e-mail@kuhni.ru">e-mail@kuhni.ru</a>
            <a class="Map-Phone" href="tel:8-800-2000-600">8-800-2000-600</a>
            <button class="JS-Modal-Button" href="#Modal-1" data-title="Заполните поля" data-button="Оформить заказ">
                Заказать звонок
            </button>
        </form>
    </div>
</section>