<section class="Examples">
    <div class="Wrapper">
        <h2 class="Section-Title">Примеры наших кухонь</h2>

        <div class="JS-Tabs Examples-Tabs">
            <ul class="JS-Tabs-Navigation Examples-Nav">
                <li class="JS-Tab Active" data-href="Tab-1">Кухонный гарнитур 1</li>
                <li class="JS-Tab" data-href="Tab-2">Кухонный гарнитур 2</li>
                <li class="JS-Tab" data-href="Tab-3">Кухонный гарнитур 3</li>
            </ul>
            <div class="JS-Tabs-Content Examples-Content">
                <ul data-tab='Tab-1'>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-1.jpg'>
                            <img src="/img/example-1.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-1.jpg'>
                            <img src="/img/example-1.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-2.jpg'>
                            <img src="/img/example-2.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-1.jpg'>
                            <img src="/img/example-1.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul data-tab='Tab-2'>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-3.jpg'>
                            <img src="/img/example-3.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-4.jpg'>
                            <img src="/img/example-4.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul data-tab='Tab-3'>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-5.jpg'>
                            <img src="/img/example-5.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="Example-Item">
                        <div class='JS-Window-Button' data-window-img='/img/example-6.jpg'>
                            <img src="/img/example-6.jpg" alt="">

                            <div class="Item-Descriptions">
                                <div>
                                    <span class="Item-Title">Кухня «Венеция»</span>
                                    <span class="Item-Info">из массива ясеня</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <button class="More JS-Modal-Button" href="#Modal-1" data-title="Смотреть больше примеров" data-button="Оформить заказ">
            Смотреть больше примеров
        </button>
    </div>
</section>