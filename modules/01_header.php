<div class="Header-Wrapper">
    <div class="Wrapper">
        <div class="Header-Top">
            <div class="Header-Logo">
                <svg>
                    <use xlink:href="#logo"></use>
                </svg>
            </div>
            <div class="Head-Text">
                <h1>Кухни на заказ по Москве и области</h1>

                <p>Производство кухонной мебели на заказ по индивидуальному <br>
                    проекту с <span class="Allotment-Text">бесплатной</span> доставкой по Москве и области</p>
            </div>
            <div class="Header-Order">
                <a class="tel" href="tel:8&#8209;800&#8209;2000&#8209;600">8&#8209;800&#8209;2000&#8209;600</a>
                <button class="JS-Modal-Button" href="#Modal-1" data-title="Заказать выезд
                    дизайнера" data-button="Оформить заказ">
                    Заказать выезд<br>
                    дизайнера бесплатно
                </button>
            </div>
        </div>
        <div class="Header-Center">
            <h2 class="Section-Title">Оставьте заявку сейчас<br>
                и получите <span class="Allotment-Text">бесплатный</span><br>
                проект кухни в 3D
            </h2>

            <div class="Header-Contacts Order-Contacts">
            <span class="Order-Title">Оставить заявку на <span class="Allotment-Text">бесплатную</span><br>
            разработку проекта 3D</span>

                <div class="Form">
                    <form action="/php/action.php" class="Form-Action">
                        
                        <input type="hidden" name="title" value="Заявка на бесплатную разработку проекта">
                        <input name="utm_medium" class="utm_medium" value="<? echo $utm_medium;?>" type="hidden">
                        <input name="utm_source" class="utm_source" value="<? echo $utm_source;?>" type="hidden">
                        <input name="utm_campaign" class="utm_campaign" value="<? echo $utm_campaign;?>" type="hidden">
                        <input name="utm_term" class="utm_term" value="<? echo $utm_term;?>" type="hidden">
                        <input name="utm_content" class="utm_content" value="<? echo $utm_content;?>" type="hidden">
                        <input name="block" class="block" value="<? echo $block;?>" type="hidden">
                        <input name="keyword" class="keyword" value="<? echo $keyword;?>" type="hidden">
                        <input name="position" class="position" value="<? echo $position;?>" type="hidden">

                        <input type="text" name="name" placeholder="Введите имя">
                        <input type="text" name="phone" placeholder="Введите телефон">
                        <button>Оставить заявку на 3D проект</button>
                        <p class="Message"></p>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>