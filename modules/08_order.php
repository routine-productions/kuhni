<section class="Order">
    <div class="Order-Blackout"></div>


    <div class="Order-Content">
        <h2 class="Section-Title">Оставьте заявку на бесплатное 3D <br>
            моделирование будущей кухни
        </h2>
        <p class="Content-Descriptions">
            Вы можете увидеть свою кухню уже сейчас, просто<br>
            оставьте заявку на бесплатный проект кухни
        </p class='Content-Descriptions'>

        <div class="Order-Form Form">
            <form action="/php/action.php" class="Form-Action">
                <input type="hidden" name="title" value="Заявка на бесплатное моделирование будущей кухни">
                <input name="utm_medium" class="utm_medium" value="<? echo $utm_medium;?>" type="hidden">
                <input name="utm_source" class="utm_source" value="<? echo $utm_source;?>" type="hidden">
                <input name="utm_campaign" class="utm_campaign" value="<? echo $utm_campaign;?>" type="hidden">
                <input name="utm_term" class="utm_term" value="<? echo $utm_term;?>" type="hidden">
                <input name="utm_content" class="utm_content" value="<? echo $utm_content;?>" type="hidden">
                <input name="block" class="block" value="<? echo $block;?>" type="hidden">
                <input name="keyword" class="keyword" value="<? echo $keyword;?>" type="hidden">
                <input name="position" class="position" value="<? echo $position;?>" type="hidden">
                <input type="text" name="name" placeholder="Введите имя">
                <input type="text" name="phone" placeholder="Введите телефон">
                <button>Оставить заявку</button>
                <p class="Message"></p>
            </form>
        </div>
    </div>

</section>