<section class="Partners">
    <div class="Wrapper">
        <h2 class="Section-Title">Наши партнёры</h2>

        <div class="Partners-Slider">
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-3.jpeg" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-4.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-5.jpg" alt="">
                    </div>
                </div>
            </div>

            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-1.png" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-3.jpeg" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-4.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="Partners-Item">
                <div>
                    <div>
                        <img src="/img/partners-5.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>