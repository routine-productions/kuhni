<section class="Reviews">
    <div class="Wrapper">
        <h2 class="Section-Title">Посмотрите отзывы наших клиентов</h2>
        <ul class="Reviews-List">
            <li class="Review-Item">
                <div class="Item-Image JS-Image-Align" data-image-ratio="16/8.5" data-image-position="center/center">
                    <img src="/img/review-1.jpg" alt="">
                </div>
                <div class="Item-Description">
                    <span class="Title">Алексей Петров</span>

                    <p>
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut
                        aliquip ex ea commodo consequat."
                    </p>
                </div>
            </li>
            <li class="Review-Item">
                <div class="Item-Image JS-Image-Align" data-image-ratio="16/8.5" data-image-position="center/center">
                    <img src="/img/review-2.jpg" alt="">
                </div>
                <div class="Item-Description">
                    <span class="Title">Алексей Петров</span>

                    <p>
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut
                        aliquip ex ea commodo consequat."
                    </p>
                </div>
            </li>
            <li class="Review-Item">
                <div class="Item-Image JS-Image-Align" data-image-ratio="16/8.5" data-image-position="center/center">
                    <img src="/img/review-3.png" alt="">
                </div>
                <div class="Item-Description">
                    <span class="Title">Алексей Петров</span>

                    <p>
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut
                        aliquip ex ea commodo consequat."
                    </p>
                </div>
            </li>
            <li class="Review-Item">
                <div class="Item-Image JS-Image-Align" data-image-ratio="16/8.5" data-image-position="center/center">
                    <img src="/img/review-4.jpg" alt="">
                </div>
                <div class="Item-Description">
                    <span class="Title">Алексей Петров</span>

                    <p>
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut
                        aliquip ex ea commodo consequat."
                    </p>
                </div>
            </li>
        </ul>

        <button class="JS-Modal-Button" href="#Modal-1" data-title=" Заказать бесплатный расчет кухни" data-button="Оформить заказ">
            Заказать бесплатный расчет кухни
        </button>
    </div>
</section>