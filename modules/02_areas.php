<section class="Areas">
    <div id="Areas-Map"></div>
    
    <div class="Areas-Info">
        <h2 class="Section-Title">
            Мы являемся<br>
            производителями!
        </h2>
        <p>
            Наши производственные площади равны<br>
            <span class="Allotment-Text">15 футбольным полям,</span>благодаря этому<br>
            наши цены <span class="Allotment-Text">ниже рыночных на 10%.</span>
        </p>

        <ul class="Slick-Areas bxslider">
            <li class="JS-Window-Button" data-window-img="/img/area-slider.jpg">
                <img src="/img/area-slider.jpg" alt="">
            </li>
            <li class="JS-Window-Button" data-window-img="/img/example-1.jpg">
                <img src="/img/example-1.jpg" alt="">
            </li>
            <li class="JS-Window-Button" data-window-img="/img/example-2.jpg">
                <img src="/img/example-2.jpg" alt="">
            </li>
        </ul>
    </div>
</section>