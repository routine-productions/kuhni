<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Кухни</title>
    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <link rel="icon" type="image/png" href="/img/favicon.png">
</head>

<body>
<?php
// Set UTM
$utm_medium = isset($_GET['utm_medium']) ? $_GET['utm_medium'] : '';
$utm_source = isset($_GET['utm_source']) ? $_GET['utm_source'] : '';
$utm_campaign = isset($_GET['utm_campaign']) ? $_GET['utm_campaign'] : '';
$block = isset($_GET['block']) ? $_GET['block'] : '';
$utm_term = isset($_GET['utm_term']) ? $_GET['utm_term'] : '';
$utm_content = isset($_GET['utm_content']) ? $_GET['utm_content'] : '';
$position = isset($_GET['position']) ? $_GET['position'] : '';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : ''; ?>
<?php require_once __DIR__ . "/img/sprite.svg"; ?>

<header>
    <?php require_once __DIR__ . "/modules/01_header-2.php"; ?>
</header>

<main>
    <?php
    require_once __DIR__ . "/modules/02_areas.php";
    require_once __DIR__ . "/modules/03_examples.php";
    require_once __DIR__ . "/modules/04_guarantees.php";
    require_once __DIR__ . "/modules/05_partners.php";
    require_once __DIR__ . "/modules/06_stock.php";
    require_once __DIR__ . "/modules/07_quality.php";
    require_once __DIR__ . "/modules/08_order.php";
    require_once __DIR__ . "/modules/09_reviews.php";
    require_once __DIR__ . "/modules/10_video-block.php";
    require_once __DIR__ . "/modules/11_questions.php";
    require_once __DIR__ . "/modules/12_map.php";
    ?>
</main>

<footer>
    <?php require_once __DIR__ . "/modules/13_footer.php"; ?>
</footer>

<?php require_once __DIR__ . "/modules/modal-order.php"; ?>

<script src="/index.min.js"></script>
<body>
</html>